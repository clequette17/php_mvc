<?php
function getPage($db)
{

// Inscrire vos contrôleurs ici
    $lesPages['accueil'] = "actionAccueil";
    $lesPages['inscrire'] = "actionInscrire";
    $lesPages['mentions'] = "actionMentions";
    $lesPages['connexion'] = "actionConnexion";
    $lesPages['deconnexion'] = "actionDeconnexion";
    $lesPages['apropos'] = "actionApropos";
    $lesPages['maintenance'] = "actionMaintenance";

    if ($db != null) {
        if (!isset($_SESSION['login'])) {
            $page = 'connexion';
        } else {
            if (isset($_GET['page'])) {
                // Nous mettons dans la variable $page, la valeur qui a été passée dans le lien
                $page = $_GET['page'];
            } else {
                // S'il n'y a rien en mémoire, nous lui donnons la valeur « accueil » afin de lui afficher une page
                //par défaut
                $page = 'accueil';
            }

            if (!isset($lesPages[$page])) {
                // Nous rentrons ici si cela n'existe pas, ainsi nous redirigeons l'utilisateur sur la page d'accueil
                $page = 'accueil';
            }
        }
    } else {
        // La base de données n'est pas accessible
        $page = 'maintenance';
    }
// La fonction envoie le contenu
    return $lesPages[$page];

}
