<?php

function actionTacheAjout($twig, $db)
{

    $tache = new Tache($db);

    $projet = new Projet($db);
    $idEquipe = $projet->selectByCode($_GET['code'])['idEquipe'];

    $equipe = new Equipe($db);

    $listedevs = $equipe->selectLesDevs($idEquipe);

    if (isset($_POST['btAjouter'])) {

        $libelle = $_POST['libelle'];
        $commentaire = $_POST['commentaire'];
        $dateD = $_POST['dateD'];
        $dateF = $_POST['dateF'];

        $tache->insert($libelle, $commentaire, $dateD, $dateF, $_GET['code']);

        $codeTache = $tache->selectMaxCode()['maxCode'];

        $dev = $_POST['developpeur'];

        foreach ($dev as $email) {

            $tache->insertDevelopper($email, $codeTache);

        }
    }

    echo $twig->render("tache-ajout.html.twig", array('codeproj' => $_GET['code'], 'listedevs' => $listedevs));
}

function actionTacheListe($twig, $db){

    $tache = new Tache($db);

    if (isset($_GET['codeT'])) {

        $tache->deleteDevelopperAll($_GET['codeT']);
        $tache->delete($_GET['codeT']);
    }

    $projet = new Projet($db);
    $leprojet = $projet->selectByCode($_GET['code']);

    $listetache = $tache->selectByCodeProj($_GET['code']);

    if (isset($_POST['btDL'])) {
        $html = $twig->render('tacheliste_pdf.html.twig', array('projet' => $leprojet, 'liste' => $listetache));
        try {
            ob_end_clean();
            $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'fr');
            $html2pdf->writeHTML($html);
            $html2pdf->output('liste_taches.pdf');
        } catch (Html2PdfException $e) {
            echo 'erreur ' . $e;
        }
    }

    echo $twig->render("tache-liste.html.twig", array('projet' => $leprojet, 'liste' => $listetache));
}

function actionTacheVoir($twig, $db)
{

    $tache = new Tache($db);

    $mestaches = $tache->selectMesTaches($_SESSION['login']);

    echo $twig->render("tache-voir.html.twig", array('mestaches' => $mestaches));
}

function actionTacheModif($twig, $db){

    $tache = new Tache($db);

    if(isset($_POST['btModifier'])){

        $libelle = $_POST['libelle'];
        $commentaire = $_POST['commentaire'];
        $dateD = $_POST['dateD'];
        $dateF = $_POST['dateF'];

        $tache->update($libelle, $commentaire, $dateD, $dateF, $_GET['code']);
    }

    $laTache = $tache->selectByCode($_GET['code']);

    $equipe = new Equipe($db);
    $projet = new Projet($db);
    $idEquipe = $projet->selectByCode($laTache['codeProj'])['idEquipe'];

    $lesDevs = $equipe->selectNotLesDevs($idEquipe, $_GET['code']);

    $lesDevsDeTache = $tache->selectDevsByCode($_GET['code']);

    echo $twig->render("tache-modif.html.twig", array('t' => $laTache, 'lesDevs' => $lesDevs, 'lesDevsDeTache' => $lesDevsDeTache));
}

function actionGererDevs($twig, $db){

    $tache = new Tache($db);

    if($_POST['action'] == "ajouter"){

        $tache->insertDevelopper($_POST['emailDev'], $_GET['code']);
    } else if($_POST['action'] == "supprimer"){

        $tache->deleteDevelopper($_POST['emailDev'], $_GET['code']);
    }

}

function actionTacheModifHeure($twig, $db)
{

    $tache = new Tache($db);

    if (isset($_POST['btModifier'])) {

        $code = $_POST['code'];
        $email = $_SESSION['login'];

        $heure = $_POST['heure'];

        $tache->updateDevelopper($email, $code, $heure);

    }

    echo $twig->render("tache-modif-heure.html.twig", array('codeTache' => $_GET['code']));
}
