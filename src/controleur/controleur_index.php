<?php

function actionAccueil($twig, $db) {
    $form = array();
    $form['valide'] = true;
    if (isset($_POST['btConnecter'])) {
        $inputEmail = $_POST['inputEmail'];
        $inputPassword = $_POST['inputPassword'];
        $utilisateur = new Utilisateur($db);
        $unUtilisateur = $utilisateur->connect($inputEmail);
        if ($unUtilisateur != null) {
            if (!password_verify($inputPassword, $unUtilisateur['mdp'])) {
                $form['valide'] = false;
                $form['message'] = 'Login ou mot de passe incorrect';
            } else {
                $_SESSION['login'] = $inputEmail;
                $_SESSION['role'] = $unUtilisateur['idRole'];
                header("Location:index.php");
            }
        } else {
            $form['valide'] = false;
            $form['message'] = 'Login ou mot de passe incorrect';
        }
    }
    echo $twig->render('index.html.twig', array('form' => $form));
}

function actionConnexion($twig, $db) {
    $form = array();
    $form['valide'] = true;
    if (isset($_POST['btConnecter'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $utilisateur = new Utilisateur($db);
        $unUtilisateur = $utilisateur->connect($email);
        if ($unUtilisateur != null) {
            if (!password_verify($password, $unUtilisateur['mdp'])) {
                $form['valide'] = false;
                $form['message'] = 'Login ou mot de passe incorrect';
            } else {
                $_SESSION['login'] = $email;
                $_SESSION['role'] = $unUtilisateur['idRole'];
                header("Location:index.php");
            }
        } else {
            $form['valide'] = false;
            $form['message'] = 'Login ou mot de passe incorrect';
        }
    }
    echo $twig->render('connexion.html.twig', array('form' => $form));
}

function actionDeconnexion($twig) {
    session_unset();
    session_destroy();
    header("Location:index.php");
}

function actionInscrire($twig, $db) {
    $form = array();
    if (isset($_POST['btInscrire'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $role = 3; // Signifie que par défaut, une personne est un simple utilisateur
        $telephone = $_POST['telephone'];
        $adresse = $_POST['adresse'];

        $siren = $_POST['siren'];
        $denomination = $_POST['denomination'];
        $telephoneE = $_POST['telephoneE'];
        $adresseE = $_POST['adresseE'];
        $cout = null;
        $form['valide'] = true;
        if ($password != $password2) {
            $form['valide'] = false;
            $form['message'] = 'Les mots de passe sont différents';
        } else {
            $utilisateur = new Utilisateur($db);
            $entreprise = new Entreprise($db);
            $exec = $utilisateur->insert($email, password_hash($password, PASSWORD_DEFAULT), $role, $nom, $prenom, $telephone, $adresse, $cout);
            $exec2 = $entreprise->insert($siren, $denomination, $telephoneE, $adresseE, $email);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Problème d\'insertion dans la table utilisateur ';
            } 
            if (!$exec2) {
                $form['valide'] = false;
                $form['message'] .= 'Problème d\'insertion dans la table entreprise ';
            }
        }

        $form['email'] = $email;
        $form['role'] = $role;
    }
    echo $twig->render('inscrire.html.twig', array('form' => $form));
}

function actionMentions($twig) {
    echo $twig->render('mentions.html.twig', array());
}

function actionApropos($twig) {
    echo $twig->render('apropos.html.twig', array());
}

function actionMaintenance($twig) {
    echo $twig->render('maintenance.html.twig', array());
}

?>
